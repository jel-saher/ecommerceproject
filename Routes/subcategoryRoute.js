const route = require("express").Router();
const subcategoryCont = require("../Controllers/subcategoryCont");

route.post("/createsubcategory", subcategoryCont.createSubcategory);
route.get("/getallsubcategory", subcategoryCont.getAllSubcategory);
route.get("/getbyidsubcategory/:id", subcategoryCont.getByIdsubcategory);
route.get("/getbynamesubcategory", subcategoryCont.getByNameSubcategory);
route.put("/updatesubcategory/:id", subcategoryCont.updateSubcategory);
route.delete("/deletesubcategory/:id", subcategoryCont.deleteSubcategory);

module.exports = route;
