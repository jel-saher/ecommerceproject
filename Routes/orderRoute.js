const route = require("express").Router();
const orderCont = require("../Controllers/orderCont");
const passport = require ("passport");
require("../Middlewares/passport_auth").passport;

route.post("/createorder", orderCont.createOrder);
route.get("/getallorder",orderCont.getAllOrder);
route.get("/getbyidorder/:id",orderCont.getByIdOrder);
route.put("/updateorder/:id",orderCont.updateOrder);
route.delete("/deleteorder/:id",orderCont.deleteOrder);
route.get("/getorderbyclient",passport.authenticate("jwt", { session: false }),orderCont.getOrderByClient);


module.exports = route;