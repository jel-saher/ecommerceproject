const route = require("express").Router();
const authController = require("../Controllers/authController");
const uploadImage = require("../Middlewares/uploadImage");
const check_auth = require("../Middlewares/check_auth");
const passport = require("passport");
require("../Middlewares/passport_auth").passport;

/* route.post("/registerclient", authController.registerClient); */
route.post(
  "/registerclient",
  uploadImage.single("image"),
  authController.registerClient
);
route.post("/login", authController.login);
route.post(
  "/refreshToken",
  passport.authenticate("jwt", { session: false }),
  authController.refreshToken
);
route.get("/verifyNow/:verificationcode", authController.verifyEmail);
route.put(
  "/updateprofil",
  uploadImage.single("image"),
  passport.authenticate("jwt", { session: false }),
  authController.updateProfile
);

// 1ere methode avec jwt
/* route.get("/profile", check_auth, authController.profile); */

//2eme methode plus pro utilisation du passport
route.get(
  "/profile",
  passport.authenticate("jwt", { session: false }),
  authController.profile
);
route.get(
  "/getallclient",

  authController.getAllClient
);
route.get("/getbyidclient/:id", authController.getByIdClient);
route.delete("/deleteclient/:id", authController.deleteClient);
route.post("/forgetpassword", authController.forgetpassword);
route.get("/resetpassword/:resetpassword",authController.resetpassword);

module.exports = route;
