const route = require("express").Router();
const productCont = require("../Controllers/productCont");
const uploadimage = require("../Middlewares/uploadImage");
const passport = require("passport");
require("../Middlewares/passport_auth").passport;

// ,passport.authenticate("jwt",{session:false})

/* route.post("/createproduct",productCont.createProduct); */


route.post(
  "/createproduct",passport.authenticate("jwt",{session:false}),
  uploadimage.array("pictures"),

  productCont.createProduct
);



// passport.authenticate("jwt", { session: false }),

route.get("/getallproduct", productCont.getAllProduct);
route.get("/getbyidproduct/:id", productCont.getByIdProduct);
route.get("/getbynameproduct", productCont.getByNameProduct);
route.put("/updateproduct/:id", productCont.updateProduct);
route.delete("/deleteproduct/:id", productCont.deleteProduct);

module.exports = route;
