const route = require("express").Router();
const providerCont = require("../Controllers/providerCont");

route.post("/createprovider", providerCont.createProvier);
route.get("/getallprovider", providerCont.getAllProvider);
route.get("/getproviderbyid/:id", providerCont.getProviderById);
route.get("/getproviderbyname", providerCont.getProviderByName);
route.put("/updateprovider/:id", providerCont.updateProvider);
route.delete("/deleteprovider/:id", providerCont.deleteProvider);

module.exports = route;
