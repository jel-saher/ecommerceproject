const route = require('express').Router();
const categoryCont = require('../Controllers/categoryCont');

route.post("/createcategory",categoryCont.createCategory);
route.get("/getallcategory",categoryCont.getAllCategory);
route.get("/getcategorybyid/:id",categoryCont.getCategoryByID);
route.get("/getcategorybyname",categoryCont.getByNameCategory);
route.put("/updatecategory/:id",categoryCont.updateCategory);
route.delete("/deletecategory/:id",categoryCont.deleteCategory);


module.exports= route;