const { connect } = require("mongoose");
const { success, error } = require("consola"); //enregistreur de console élégant pour node.js (utilisation des badges)

const DB = process.env.APP_DB;

const connectdb = async () => {
  try {
    await connect(DB);
    success({
      message: `Successfully connected with the Database \n${DB}`,
      badge: true,
    });
  } catch (err) {
    error({
      message: `Unable to connect with Database \n${err}`,
      badge: true,
    });
    connectdb();
  }
};

module.exports = connectdb();
