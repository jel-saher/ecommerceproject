const jwt = require("jsonwebtoken");
const SECRET = process.env.APP_SECRET;

const check_auth = async (req, res) => {
  try {
    const token = req.headers["authorization"];
    if (!token) {
      return res.status(403).json({ message: "no token" });
    }
    const decord = jwt.verify(token, SECRET);
    return res.status(200).json({
      message: "auth",
      data: decord,
    });
  } catch (error) {
    res.status(500).json({
      message: "Requete non authentifiée",
      status: 500,
    });
  }
};

module.exports = check_auth;
