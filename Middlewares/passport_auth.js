const { Strategy, ExtractJwt } = require("passport-jwt");
const passport = require("passport");
const SECRET = process.env.APP_SECRET;
const User = require("../Models/User");

// choisir la format du token
var options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET,
};

passport.use(
  new Strategy(options, async ({ id }, done) => {
    try {
      const user = await User.findById(id).populate("orders");
      if (!user) {
        // done(null,"User not found");
        throw new Error("User not found...");
      }
      done(null, user);
      //pour remplacer la methode suivante
      /*   done.status(200).json({
          message : "user",
          data : user
      }) */
    } catch (error) {
      done(null, error.message);
      // ancien méthode
      /* done.status(500).json({
        message: error.message,
        status: 500,
      }); */
    }
  })
);
