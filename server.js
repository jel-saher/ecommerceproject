const express = require("express");
const cors = require("cors");
require("dotenv").config();
const { success, error } = require("consola");
const db = require("./Config/db");

const PORT = process.env.APP_PORT;
const DOMAIN = process.env.APP_DOMAIN;
const passport = require("passport");

const registerclient = require("./Routes/authRoute");
const category = require("./Routes/categoryRoute");
const subcategory = require("./Routes/subcategoryRoute");
const product = require("./Routes/productRoute");
const order = require("./Routes/orderRoute");
const provider = require("./Routes/providerRoute");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(passport.initialize()); //utilisation du module passport
app.use("/", registerclient);
app.use("/category", category);
app.use("/subcategory", subcategory);
app.use("/product", product);
app.use("/order", order);
app.use("/provider", provider);

/* affichage image */
app.get("/getfile/:image", function (req, res) {
  res.sendFile(__dirname + "/storages/" + req.params.image);
});

// START LISTENING FOR THE SERVER ON PORT
app.listen(PORT, async () => {
  try {
    success({
      message:
        `Server started on PORT ${PORT} ` + `URL : http://localhost:${PORT}`,
      badge: true,
    });
  } catch (err) {
    error({ message: "error with server" + err.message, badge: true });
  }
});
