const mongoose = require("mongoose"); // Erase if already required
const Usermodel = require("../Models/User");

var providerSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },

    email: {
      type: String,
      unique: true,
    },

    adress: {
      type: String,
    },
    mobile: {
      type: String,
      required: true,
    },
    products: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Product",
      },
    ],
  },
  { timestamps: true }
);

//Export the model
module.exports = mongoose.model("Provider", providerSchema);
