const mongoose = require("mongoose"); // Erase if already required

// Declare the Schema of the Mongo model
var subcategorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },

    description: {
      type: String,
    },
    // ref to id category
    category: {
      type: mongoose.Types.ObjectId,
      ref: "Category",
      required: false,
    },
    products: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Product",
      },
    ],
  },
  { timestamps: true }
);

//Export the model
module.exports = mongoose.model("SubCategory", subcategorySchema);
