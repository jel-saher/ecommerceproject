const { string } = require("i/lib/util");
const mongoose = require("mongoose"); // Erase if already required

// Declare the Schema of the Mongo model
var gallerySchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true,
  },
  description: {
    type: String,
    trim: true,
    required: true,
  },
});

var productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    prix: {
      type: String,
      required: true,
    },
    quantite: {
      type: String,
      required: true,
    },
    color: {
      type: String,
      required: true,
    },
    ref: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    subcategory: {
      type: mongoose.Types.ObjectId,
      ref: "SubCategory",
      required: false,
    },
    provider: {
      type: mongoose.Types.ObjectId,
      ref: "Provider",
      required: false,
    },
    galleries: [gallerySchema],
  },
  { timestamps: true }
);

//Export the model
module.exports = mongoose.model("Product", productSchema);
