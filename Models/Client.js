const mongoose = require("mongoose");
//import du modele user pour l'utiliser au niveau heritage pour le client
const Usermodel = require("../Models/User");

var clientSchema = new mongoose.Schema({
  firstname: {
    type: String,
    // required: true,
  },
  lastname: {
    type: String,
    // required: true,
  },
  adresse: {
    type: String,
    
  },
  image: {
    type: String,
    required : false,
  },
  orders: [
    {
      type: mongoose.Types.ObjectId,
      ref: "Order",
    },
  ],
});

//Export the model
// heritage modele user pour le client (discriminator);
module.exports = Usermodel.discriminator("Client", clientSchema);
