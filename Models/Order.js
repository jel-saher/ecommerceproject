const mongoose = require("mongoose");

const itemorderSchema = new mongoose.Schema({
  quantite: {
    type: Number,
    required: true,
  },
  prix: {
    type: String,
    required: true,
  },

  product: {
    type: mongoose.Types.ObjectId,
    ref: "Product",
    required: false,
  },
});

const orderSchema = new mongoose.Schema({
  quantite: {
    type: Number,
    required: true,
  },
  prixTotale: {
    type: String,
    required: true,
  },

  paye: {
    type: Boolean,
    default: false,
  },
  client: {
    type: mongoose.Types.ObjectId,
    ref: "Client",
    required: true,
  },
  itemorder: [itemorderSchema],
},
{ timestamps: true }
);

//Export the model
module.exports = mongoose.model("Order", orderSchema);
