const { string } = require("i/lib/util");
const mongoose = require("mongoose"); // Erase if already required

var userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },

  password: {
    type: String,
    required: true,
  },

  verify: {
    type: Boolean,
    default: false,
  },

  verificationcode: {
    type: String,
  },

  resetpassword: {
    type: String,
    required: false,
  },
});

//Export the model
module.exports = mongoose.model("User", userSchema);
