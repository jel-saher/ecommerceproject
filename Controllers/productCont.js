const Product = require("../Models/Product");
const SubCategory = require("../Models/SubCategory");
const Provider = require("../Models/Provider");

const createProduct = async (req, res) => {
  try {
    req.body["galleries"] =
      req.files.length <= 0
        ? []
        : req.files.map(function (file) {
            return {
              name: file.filename,
              description: "Pictures of product",
            };
          });
    const newProduct = new Product(req.body);
    const product = await newProduct.save();
    await SubCategory.findByIdAndUpdate(req.body.subcategory, {
      $push: { products: product },
    });
    await Provider.findByIdAndUpdate(req.body.provider, {
      $push: { products: product },
    });
    res.status(200).json({ message: "Product created", data: product });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getAllProduct = async (req, res) => {
  try {
    const liste = await Product.find({}).populate({path:"subcategory",populate:{path:"category"}}).populate("provider");
    res.status(200).json({ message: "Liste Product", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByIdProduct = async (req, res) => {
  try {
    const byidproduct = await Product.findById({ _id: req.params.id })
      .populate("subcategory")
      .populate("provider");
    res.status(200).json({ meessage: "Product", data: byidproduct });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByNameProduct = async (req, res) => {
  try {
    const bynameproduct = await Product.find({ name: req.query.name }).populate(
      "subcategory"
    );
    res.status(200).json({ meessage: "Product", data: bynameproduct });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateProduct = async (req, res) => {
  try {
    await Product.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({ meessage: "Product Updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteProduct = async (req, res) => {
  try {
    const prod = await Product.findById({ _id: req.params.id });
    //prod.subcategory ref to Product.subcategory (subcategory attribu creer au niveau schema Product)
    await SubCategory.findByIdAndUpdate(prod.subcategory, {
      $pull: { products: prod._id },
    });
    await Provider.findByIdAndUpdate(prod.provider, {
      $pull: { products: prod._id },
    });

    await Product.deleteOne({ _id: req.params.id });
    res.status(200).json({ message: "Product deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createProduct,
  getAllProduct,
  getByIdProduct,
  getByNameProduct,
  updateProduct,
  deleteProduct,
};
