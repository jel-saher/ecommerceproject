const Subcategory = require("../Models/SubCategory");
const Category = require("../Models/Category");

const createSubcategory = async (req, res) => {
  try {
    const newSubcategory = new Subcategory(req.body);
    const subcategory = await newSubcategory.save();
    await Category.findByIdAndUpdate(req.body.category, {
      $push: { subcategories: subcategory },
    });
    res.status(200).json({ meassge: "SubCategory created", data: subcategory });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getAllSubcategory = async (req, res) => {
  try {
    const liste = await Subcategory.find({})
      .populate("products")
      .populate("category");
    res.status(200).json({ meassge: "List SubCategory", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
const getByIdsubcategory = async (req, res) => {
  try {
    const subcat = await Subcategory.findById({ _id: req.params.id })
      .populate("products")
      .populate("category");

    res.status(200).json({ meassge: "SubCategory : ", data: subcat });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByNameSubcategory = async (req, res) => {
  try {
    const sub = await Subcategory.find({ name: req.query.name }).populate(
      "products"
    );
    res.status(200).json({ meassge: "SubCategory : ", data: sub });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateSubcategory = async (req, res) => {
  try {
    await Subcategory.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({ message: "Subcategory Updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteSubcategory = async (req, res) => {
  try {
    const subcat = await Subcategory.findById({ _id: req.params.id });
    //subcat.category ref to Subcategory.category (category attribu creer au niveau schema Subcategory)
    await Category.findByIdAndUpdate(subcat.category, {
      $pull: { subcategories: subcat._id },
    });
    await Subcategory.deleteOne({ _id: req.params.id });

    res.status(200).json({ message: "Subcategory Deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createSubcategory,
  getAllSubcategory,
  getByIdsubcategory,
  getByNameSubcategory,
  updateSubcategory,
  deleteSubcategory,
};
