const Client = require("../Models/Client");
const User = require("../Models/User");
//import module bcryp pour pourvoir crypte le password (npm i bcrypt)
const bcryp = require("bcrypt");

const DOMAIN = process.env.APP_DOMAIN;
const SECRET = process.env.APP_SECRET;
const jwt = require("jsonwebtoken");
//pour l'utilisation des token (npm i jsonwebtoken)

const { randomBytes } = require("crypto");
const { join } = require("path");
const { use } = require("../Routes/authRoute");

//IMPORT DU MODULE NODEMAILER (npm i nodemailer)
const nodemailer = require("nodemailer");
const { find } = require("../Models/User");

//CREER UN TRANSPORTEUR AVEC LE MODULE NODEMAILER AVEC LA FONCTION CREATETRANSPORT
const transporteur = nodemailer.createTransport({
  service: "gmail",
  auth: {
    //importer user get pass de .env pour cacher les données personnelles
    user: process.env.APP_USER,
    pass: process.env.APP_PASS,
  },
});
const registerClient = async (req, res) => {
  try {
    req.body["image"] = req.file.filename;
    //utiliser bcryp pour crypter le password avec methode hashSync
    const password = bcryp.hashSync(req.body.password, 10);
    const newClient = new Client({
      ...req.body,
      password,
      verificationcode: randomBytes(6).toString("hex"), ///???
    });
    const client = await Client.create(newClient);
    res.status(201).json({
      message: "Your account is created, please verify your email adress",
    });
    transporteur.sendMail(
      {
        to: client.email,
        subject: "welcome " + client.firstname,
        text: "Bonjour MR",
        html: `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Welcome Email</title>
        </head>
        <body>
            <h2>
                Hello ${client.firstname}
            </h2>
            <p>We are glad to have you on bord at ${client.email}</p>
            <a href="${DOMAIN}/verifyNow/${client.verificationcode}">Verify email</a>
        </body>
        </html>`,
      },
      //fonction pour afficher msg lors de la creation de client lors de lenvoi du email
      function (err, info) {
        if (err) {
          console.log("error : " + err.message);
        } else {
          console.log("email send : " + info.res);
        }
      }
    );
  } catch (error) {
    res.status(500).json({
      message: error.message,
      status: 500,
    });
  }
};

const verifyEmail = async (req, res) => {
  try {
    const verificationcode = req.params.verificationcode;
    const user = await User.findOne({ verificationcode });
    user.verify = true;
    user.verificationcode = undefined;
    user.save();
    res.sendFile(join(__dirname, "../Templates/verificationsucces.html"));
  } catch (error) {
    res.sendFile(join(__dirname, "../Templates/errors.html"));
  }
};

const login = async (req, res) => {
  try {
    const user = await Client.findOne({ email: req.body.email });
    if (!user) {
      return res.status(500).json({
        message: "Invalid Email",
      });
    }
    /* bcryp.compare(req.body.password, user.password) */
    const valid = await bcryp.compareSync(req.body.password, user.password);
    if (!valid) {
      return res.status(500).json({
        message: "Password false",
      });
    }

    if (user.verify === false) {
      res.status(500).json({
        message: "Your Account Is Not Verified",
      });
    } else {
      const token = jwt.sign({ id: user._id, user: user }, SECRET, {
        expiresIn: "24h",
      });
      const result = {
        email: user.email,
        user: user,
        token: token,
        expiresIn: 1,
      };
      res.status(200).json({
        message: user.firstname + " is logged",
        ...result,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: error.message,
      status: 500,
    });
  }
};

const forgetpassword = async (req, res) => {
  try {
    const user = await Client.findOne({ email: req.body.email });
    console.log(user);
    if (!user) {
      return res.status(500).json({ message: "Your email does not exist" });
    }
    const token = jwt.sign({ id: user._id }, SECRET, {
      expiresIn: "2h",
    });
    await Client.findOneAndUpdate({ email: req.body.email }, { resetpassword: token });
    transporteur.sendMail(
      {
        to: user.email,
        subject: "Forget Password",
        text: "Bonjour MR",
        html: `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Welcome Email</title>
        </head>
        <body>
            <h2>
                Hello ${user.firstname}
            </h2>
            <p>We are glad to have you on bord at ${user.email}</p>
            <a href="${DOMAIN}/resetpassword/${token}">Reset Password</a>
        </body>
        </html>`,
      },
      //fonction pour afficher msg lors de la creation de client lors de lenvoi du email
      function (err, info) {
        if (err) {
          console.log("error : " + err.message);
        } else {
          console.log("email send : " + info.res);
        }
      }
    );
    return res.status(200).json({ message: "email send" });
  } catch (error) {}
};

const resetpassword = async (req, res) => {
  try {
    const resetpassword = req.params.resetpassword;
    jwt.verify(resetpassword, SECRET, async (err) => {
      if (err) {
        return res.json({ error: "incorrect token or it is expired" });
      }

      const user = await User.findOne({ token: resetpassword });
      const password = bcryp.hashSync(req.body.newpassword, 10);
      user.password = password;
      user.save();
      res.status(200).json({ message: "password modified" });
    });
  } catch (error) {
    res.status(500).json({ message: error.meessage });
  }
};

const refreshToken = async (req, res) => {
  try {
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const profile = async (req, res) => {
  try {
    const user = await req.user;
    res.status(200).json({ message: "profile identify", data: user });
  } catch (error) {
    res.status(500).json({
      message: error.message,
      status: 500,
    });
  }
};

const getAllClient = async (req, res) => {
  try {
    const liste = await Client.find({});
    res.status(200).json({ message: "Liste Client", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateProfile = async (req, res) => {
  try {
    await Client.updateOne({ _id: req.user._id }, req.body);
    res.status(200).json({ message: "Profil Updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByIdClient = async (req, res) => {
  try {
    const byidclient = await Client.findById({ _id: req.params.id });
    res.status(200).json({ meessage: "Client : ", data: byidclient });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteClient = async (req, res) => {
  try {
    await Client.deleteOne({ _id: req.params.id });
    res.status(200).json({ message: "Client deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  registerClient,
  verifyEmail,
  login,
  refreshToken,
  profile,
  updateProfile,
  getAllClient,
  getByIdClient,
  deleteClient,
  forgetpassword,
  resetpassword,
};
