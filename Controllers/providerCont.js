const Product = require("../Models/Product");
const Provider = require("../Models/Provider");

const createProvier = async (req, res) => {
  try {
    const newProvider = new Provider(req.body);
    const provider = await Provider.create(newProvider);
    res.status(200).json({ message: "New provider created", data: provider });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getAllProvider = async (req, res) => {
  try {
    const listprovider = await Provider.find({}).populate("products");
    res
      .status(200)
      .json({ message: "List Of Provider : ", data: listprovider });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getProviderById = async (req, res) => {
  try {
    const provider = await Provider.findById({ _id: req.params.id });
    res.status(200).json({ message: "Provider : ", data: provider });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getProviderByName = async (req, res) => {
  try {
    const provider = await Provider.find({ name: req.query.name });
    res.status(200).json({ message: "Provider : ", data: provider });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateProvider = async (req, res) => {
  try {
    await Provider.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({ message: "Provider updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteProvider = async (req, res) => {
  try {
    await Provider.deleteOne({ _id: req.params.id });
    res.status(200).json({ message: "Provider Deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createProvier,
  getAllProvider,
  getProviderById,
  getProviderByName,
  updateProvider,
  deleteProvider,
};
