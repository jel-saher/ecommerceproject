const Order = require("../Models/Order");
const Client = require("../Models/Client");

const createOrder = async (req, res) => {
  try {
    /* req.body["itemorder"] = {
      product: req.body.product,
      qte: req.body.qte,
      prix: req.body.prix,
    }; */
    const newOrder = new Order(req.body);
    const order = await newOrder.save();
    await Client.findByIdAndUpdate(req.body.client, {
      $push: { orders: order },
    });
    res.status(200).json({ message: "Order created", data: order });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getAllOrder = async (req, res) => {
  try {
    const liste = await Order.find().populate("client");
    res.status(200).json({ message: "All Orders : ", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByIdOrder = async (req, res) => {
  try {
    const byId = await Order.findById({ _id: req.params.id }).populate(
      "client"
    );
    res.status(200).json({ message: " Order : ", data: byId });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateOrder = async (req, res) => {
  try {
    await Order.findByIdAndUpdate({ _id: req.params.id }, req.body);
    res.status(200).json({ message: "Order updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteOrder = async (req, res) => {
  try {
    const order = await Order.findById({ _id: req.params.id });
    await Client.findByIdAndUpdate(order.client, {
      $pull: { orders: order._id },
    });
    await Order.deleteOne({ _id: req.params.id });
    res.status(200).json({ message: "Order deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
const getOrderByClient = async(req,res)=>{
    try {
      const order = await req.user.orders
      res.status(200).json({ data : order });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  }

module.exports = {
  createOrder,
  deleteOrder,
  getAllOrder,
  getByIdOrder,
  updateOrder,
  getOrderByClient
};
